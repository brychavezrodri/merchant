#!/bin/bash

CURDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
TMPDIR=$CURDIR/tmp

rm -rf $TMPDIR && mkdir $TMPDIR

php $CURDIR/lib/generar-categorias-urls.php
$CURDIR/bin/aria2c -d $TMPDIR -i $TMPDIR/categorias-urls.txt > $TMPDIR/categorias-urls.log

php $CURDIR/lib/generar-paginas-urls.php
$CURDIR/bin/aria2c -d $TMPDIR -i $TMPDIR/paginas-urls.txt > $TMPDIR/paginas-urls.log

php $CURDIR/lib/generar-productos-feed.php

cp -f $TMPDIR/productos.csv $CURDIR/productos.csv
cp -f $TMPDIR/productos.rss $CURDIR/productos.rss
