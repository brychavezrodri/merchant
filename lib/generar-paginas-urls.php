<?php

define('TEMP_DIR', dirname(__DIR__) . '/tmp');


// --------------------------------------------------

function get_products_count ($html) {
	$count = 0;

	$search = '"resultado-busca-numero"';
	$ref    = mb_strpos($html, $search);
	$part   = mb_substr($html, $ref + mb_strlen($search), 100);

	if (preg_match(':<span class="value">(\d+)</span>:', $part, $matches)) {
		$count = intval($matches[1]);
	}

	return $count;
}


function get_categories_count () {
	$categorias_count = [];

	$files = glob(TEMP_DIR . '/category-*.html');

	foreach ($files as $file) {
		$categoria_id = preg_replace('/.*-(\d+).*/', '${1}', pathinfo($file, PATHINFO_FILENAME));

		$count = get_products_count(file_get_contents($file));

		if ($count === 0) {
			continue;
		}

		if (isset($categorias_count[$categoria_id])) {
			$categorias_count[$categoria_id] += $count;
		} else {
			$categorias_count[$categoria_id] = $count;
		}
	}

	return $categorias_count;
}


// --------------------------------------------------

$paginas_urls_file = TEMP_DIR . '/paginas-urls.txt';

$fh = fopen($paginas_urls_file, 'w');

$categorias_count = get_categories_count();
$categorias = require TEMP_DIR . '/categorias.inc.php';

$productos_x_pagina = 50;

$pagina_url = 'https://tienda.plazavea.com.pe/api/catalog_system/pub/products/search?fq=C:/%s/&_from=%s&_to=%s';

foreach ($categorias_count as $categoria_id => $count) {
	if ($count == 0) {
		continue;
	}

	$paginas = ceil($count / $productos_x_pagina);

	for ($pagina = 1; $pagina <= $paginas; $pagina++) {
		$desde = ($pagina - 1) * $productos_x_pagina;
		$hasta = $desde + $productos_x_pagina - 1;

		if ($desde > 2500) {
			break;
		}

		$categoria_ruta = $categorias[$categoria_id]['route'];

		$url      = sprintf($pagina_url, $categoria_ruta, $desde, $hasta);
		$filename = "pagina__{$categoria_id}__{$desde}-{$hasta}.json";

		$download_entry = sprintf("%s\n\tout=%s\n\n", $url, $filename);

		fwrite($fh, $download_entry);
	}
}

fclose($fh);
