<?php

define('TEMP_DIR', dirname(__DIR__) . '/tmp');


// --------------------------------------------------

function parse_products ($entry) {
	global $google_product_categories;

	$products = [];

	$nombre = limpiar_codigos(preg_replace('/\s+/', ' ', $entry['productName']));
	$marca  = empty($entry['brand']) ? 'Otros' : $entry['brand'];

	$categoria      = get_producto_categoria($entry);
	$categorias_ids = get_producto_categorias_ids($entry);
	$categoria_id   = $categorias_ids[0];

	$google_product_category = '';

	foreach ($categorias_ids as $_cat_id) {
		if (isset($google_product_categories[$_cat_id]) &&
			$google_product_categories[$_cat_id]
		) {
			$google_product_category = $google_product_categories[$_cat_id];
		}
	}

	foreach ($entry['items'] as $item) {
		$product = array(
			'id'           => $item['itemId'],
			'url'          => $entry['link'],
			'marca'        => $marca,
			'nombre'       => $nombre,
			'categoria'    => $categoria,
			'categoria_id' => $categoria_id,
			'descripcion'  => '',
			'imagen'       => '',

			'precio_lista'  => null,
			'precio_normal' => 0,
			'precio_oferta' => null,
			'precio_final'  => 0,
			'descuento'     => 0,

			'google_product_category' => $google_product_category,
		);

		// tomar la primera imagen
		if (isset($item['images'], $item['images'][0])) {
			$image = $item['images'][0];
			$product['imagen'] = $image['imageUrl'];
		}

		// tiene que tener "sellers"
		if (!isset($item['sellers'], $item['sellers'][0])) {
			continue;
		}

		$commertialOffer = $item['sellers'][0]['commertialOffer'];

		// precio normal
		$product['precio_normal'] = floatval($commertialOffer['Price']);
		$product['precio_final'] = $product['precio_normal'];

		// precio lista
		$precio_lista = floatval($commertialOffer['ListPrice']);

		if ($precio_lista > $product['precio_normal']) {
			$product['precio_lista'] = $precio_lista;
		}

		// solo productos con stock
		if (!intval($commertialOffer['AvailableQuantity'])) {
			continue;
		}

		// solo productos con precio
		if ($product['precio_normal'] == 0) {
			continue;
		}

		// buscar descuento
		if (isset($commertialOffer['Teasers'])) {
			foreach ($commertialOffer['Teasers'] as $teaser) {
				if (!isset(
					$teaser['<Conditions>k__BackingField'],
					$teaser['<Conditions>k__BackingField']['<Parameters>k__BackingField'],
					$teaser['<Effects>k__BackingField'],
					$teaser['<Effects>k__BackingField']['<Parameters>k__BackingField']
				)) {
					continue;
				}

				$es_tarjeta_oh = false;

				foreach ($teaser['<Conditions>k__BackingField']['<Parameters>k__BackingField'] as $parameter) {
					if (
						isset($parameter['<Name>k__BackingField']) &&
						$parameter['<Name>k__BackingField'] == 'PaymentMethodId' &&
						isset($parameter['<Value>k__BackingField']) &&
						preg_match('/(^|,)(501|502)(,|$)/', $parameter['<Value>k__BackingField']) // <---| Tarjeta Oh!
					) {
						$es_tarjeta_oh = true;
						break;
					}
				}

				if (!$es_tarjeta_oh) {
					continue;
				}

				foreach ($teaser['<Effects>k__BackingField']['<Parameters>k__BackingField'] as $parameter) {
					if (isset($parameter['<Name>k__BackingField']) && isset($parameter['<Value>k__BackingField'])) {
						if ($parameter['<Name>k__BackingField'] == 'MaximumUnitPriceDiscount') {
							$precio_oferta = round(floatval($parameter['<Value>k__BackingField']) / 100, 2);
							$descuento_monto = $product['precio_normal'] - $precio_oferta;
							$descuento_porcentaje = 100 * $descuento_monto / $product['precio_normal'];

							if ($precio_oferta < $product['precio_normal']) {
								$product['precio_oferta'] = $precio_oferta;
								$product['descuento'] = $descuento_porcentaje;
							}

						} else if ($parameter['<Name>k__BackingField'] == 'PromotionalPriceTableItemsDiscount') {
							$descuento_monto = floatval($parameter['<Value>k__BackingField']);
							$descuento_porcentaje = 100 * $descuento_monto / $product['precio_normal'];

							if ($descuento_porcentaje < 100) {
								$product['precio_oferta'] = $product['precio_normal'] - $descuento_monto;
								$product['descuento'] = $descuento_porcentaje;
							}

						} else if ($parameter['<Name>k__BackingField'] == 'PercentualDiscount') {
							$descuento_porcentaje = floatval($parameter['<Value>k__BackingField']);
							$descuento_monto = round($product['precio_normal'] * $descuento_porcentaje / 100, 2);

							if ($descuento_porcentaje < 100) {
								$product['precio_oferta'] = $product['precio_normal'] - $descuento_monto;
								$product['descuento'] = $descuento_porcentaje;
							}
						}
					}
				}
			}
		}

		if ($product['precio_oferta'] && $product['precio_oferta'] > 0) {
			$product['precio_final'] = $product['precio_oferta'];
		} else {
			$product['precio_oferta'] = null;
		}

		$products[] = $product;
	}

	return $products;
}


function get_producto_categorias_ids ($product) {
	$categorias_ids = [];

	if (isset($product['categoriesIds']) && is_array($product['categoriesIds'])) {
		foreach ($product['categoriesIds'] as $str) {
			$ids = explode('/', trim($str, '/'));

			if (count($ids) > count($categorias_ids)) {
				$categorias_ids = $ids;
			}
		}
	}

	return array_reverse($categorias_ids);
}


function get_producto_categoria ($product) {
	$categoria = '';

	if (isset($product['categoryId']) && isset($product['categories']) && isset($product['categoriesIds'])) {
		$cat_id = strval($product['categoryId']);

		foreach ($product['categoriesIds'] as $i => $line) {
			$cats_ids = explode('/', trim($line, '/'));

			if (count($cats_ids) && $cat_id == $cats_ids[count($cats_ids) - 1]) { // comparar con el nivel mas específico
				$categories = explode('/', trim($product['categories'][$i], '/'));
				$categoria = array_pop($categories);
				break;
			}
		}
	}

	if ($categoria == '') {
		$line = sprintf("%s => %s [%s] [%s]\n", $product['productId'], $product['categoryId'], implode(',', $product['categoriesIds']), implode(',', $product['categories']));
		file_put_contents(__DIR__ . '/missing.txt', $line, FILE_APPEND);
	}

	return $categoria;
}


function limpiar_codigos ($str) {
	if (preg_match_all(': ([/A-Z0-9-]{4,}):', $str, $matches)) {
		if (preg_match('/WWE 2K\d{2}/', $str)) {
			return $str;
		}

		if (preg_match('/ \d+(G|GB|T|TB|MB)-\d+(G|GB|T|TB|MB)/', $str)) {
			return $str;
		}

		$codes = [];

		foreach ($matches[1] as $code) {
			if (preg_match('/^\d+(G|GB|T|TB|MB|CM|W|MP|L|KG)$/', $code)) {
				continue;
			}

			if (preg_match('/^CI(3|5|7)-(6|7|8|9)$/', $code)) {
				continue;
			}

			if (preg_match('/[A-Z]/', $code) && preg_match('/[0-9]/', $code)) {
				$codes[] = " {$code}";
			}
		}

		if (count($codes)) {
			$str = str_replace($codes, '', $str);
		}
	}

	return $str;
}


function build_feed_item ($feed, $product) {
	$item = [];

	foreach (['precio_normal', 'precio_oferta', 'precio_final'] as $k) {
		$product[$k] = $product[$k] ? $feed['format_price']($product[$k]) : null;
	}

	foreach ($feed['columns'] as $a => $b) {
		if (is_callable($b)) {
			$item[$a] = $b($product);

		} else if (substr($b, 0, 1) == '@') {
			$value = $product[ltrim($b, '@')];

			if ($a === 'sale_price' && is_null($value)) {
				continue;
			}

			$item[$a] = is_null($value) ? '' : $value;

		} else {
			$item[$a] = $b;
		}
	}

	return $item;
}


function write_produtcs ($items) {
	global $product_ids, $feeds;

	foreach ($items as $item) {
		$products = parse_products($item);

		foreach ($products as $product) {
			$id = $product['id'];

			if (isset($product_ids[$id])) {
				continue;
			} else {
				$product_ids[$id] = 1;
			}

			//
			foreach ($feeds as $feed) {
				$item = build_feed_item($feed, $product);

				if ($feed['skip_empty_google_product_category'] && empty($product['google_product_category'])) {
					continue;
				}

				$feed['write_item']($feed['fh'], $item);
			}
		}
	}
}


// --------------------------------------------------

$generate_empty = isset($argv) && in_array('generate-empty', $argv);

$product_ids = [];

$google_product_categories = require __DIR__ . '/google_product_categories-mappgin.inc.php';

$feeds = array(
	'csv' => array(
		'file' => 'productos.csv',

		'columns' => array(
			'id'                      => '@id',
			'title'                   => '@nombre',
			'description'             => '@nombre',
			'link'                    => '@url',
			'image_link'              => '@imagen',
			'price'                   => '@precio_final',
			'brand'                   => '@marca',
			'product_type'            => '@categoria',
			'google_product_category' => '@google_product_category',
			'availability'            => 'in stock',
			'condition'               => 'new',
		),

		'format_price' => function ($amount) {
			return number_format(floatval($amount), 2, '.', ',') . ' PEN';
		},

		'skip_empty_google_product_category' => false,

		'write_header' => function ($fh, $columns) {
			fputcsv($fh, array_keys($columns));
		},

		'write_item' => function ($fh, $item) {
			fputcsv($fh, $item);
		},

		'write_footer' => function ($fh) {},
	),

	'rss' => array(
		'file' => 'productos.rss',

		'columns' => array(
			'id'                      => '@id',
			'title'                   => '@nombre',
			'description'             => '@nombre',
			'brand'                   => '@marca',
			'product_type'            => '@categoria',
			'price'                   => '@precio_normal',
			'sale_price'              => '@precio_oferta',
			'link'                    => '@url',
			'image_link'              => '@imagen',

			'availability'            => 'in stock',
			'condition'               => 'new',
			'google_product_category' => '2271',
		),

		'format_price' => function ($amount) {
			return number_format(floatval($amount), 2, '.', ',');
		},

		'skip_empty_google_product_category' => false,

		'write_header' => function ($fh) {
			fwrite($fh,
				'<?xml version="1.0"?>' .
				'<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">' .
				'<channel>' .
				'<title>PlazaVea</title>' .
				'<link>https://tienda.plazavea.com.pe</link>'
			);
		},

		'write_item' => function ($fh, $item) {
			$item_xml_str = "<item>\n";

			foreach ($item as $k => $v) {
				$item_xml_str .= sprintf('<g:%1$s>%2$s</g:%1$s>', $k, htmlspecialchars($v)) . "\n";
			}

			$item_xml_str .= "</item>\n";

			fwrite($fh, $item_xml_str);
		},

		'write_footer' => function ($fh) {
			fwrite($fh, '</channel></rss>');
		},
	)
);

foreach ($feeds as $k => $feed) {
	$fh = fopen(TEMP_DIR . '/' . $feed['file'], 'w');
	$feed['write_header']($fh, $feed['columns']);
	$feeds[$k]['fh'] = $fh;
}


if ($generate_empty) {
	write_produtcs([]);

} else {
	$data_files = glob(TEMP_DIR . '/pagina__*.json');

	foreach ($data_files as $file) {
		write_produtcs(json_decode(file_get_contents($file), true));
	}
}

foreach ($feeds as $feed) {
	$feed['write_footer']($feed['fh']);
	fclose($feed['fh']);
}
