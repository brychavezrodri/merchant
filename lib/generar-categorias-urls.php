<?php

define('TEMP_DIR', dirname(__DIR__) . '/tmp');


// --------------------------------------------------

function wget ($url) {
	$ch = curl_init($url);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);

	$data = curl_exec($ch);

	curl_close($ch);

	return json_decode($data, true);
}

function findEdges ($parent_id, $nodes, &$store) {
	foreach ($nodes as $node) {
		if (isset($node['visible']) && $node['visible'] === false) {
			continue;
		}

		if ($parent_id) {
			$route = sprintf('%s/%s', $parent_id, $node['id']);
		} else {
			$route = $node['id'];
		}

		if (isset($node['children']) && is_array($node['children']) && count($node['children']) > 0) {
			findEdges($route, $node['children'], $store);

		} else if (
			isset($node['idCategoriaAgregaCompra']) &&
			isset($node['idCategoriaRelacionada']) &&
			$node['idCategoriaAgregaCompra'] !== $node['idCategoriaRelacionada']
		) {
			continue;

		} else {
			$store[$node['id']] = [
				'route' => $route,
				'url'   => $node['url'],
			];
		}
	}
}

function get_categorias ($allowed_top_categories = null) {
	$categorias_data = wget('https://tienda.plazavea.com.pe/api/catalog_system/pub/category/tree/3/');

	if ($allowed_top_categories && count($allowed_top_categories)) {
		foreach ($categorias_data as $i => $item) {
			if (!in_array(intval($item['id']), $allowed_top_categories)) {
				unset($categorias_data[$i]);
			}
		}
	}

	$categorias = [];

	findEdges(null, $categorias_data, $categorias);

	ksort($categorias);

	return $categorias;
}


// --------------------------------------------------


$categorias_file = TEMP_DIR . '/categorias.inc.php';
$categorias_urls = TEMP_DIR . '/categorias-urls.txt';

$allowed_top_categories = [
	679, // Electro
];

$categorias = get_categorias($allowed_top_categories);

$fh = fopen($categorias_urls, 'w');

foreach ($categorias as $categoria_id => $categoria) {
	$download_entry = sprintf("%s\n\tout=category-%s.html\n\n", $categoria['url'], $categoria_id);
	fwrite($fh, $download_entry);
}

fclose($fh);

file_put_contents($categorias_file, sprintf('<?php return %s;', var_export($categorias, true)));
