<?php

/*
Sobre las categorías de ecommerce en Google:
https://support.google.com/merchants/answer/6324436?hl=es

Lista de categorías disponibles:
https://www.google.com/basepages/producttype/taxonomy-with-ids.es-ES.txt
*/

return [
	'id-de-categoria-en-ecommerce' => 'id-correspondiente-de-categoria-en-google',
];
