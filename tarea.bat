@ECHO off

SET ARIA2CD=bin\aria2c.exe --ca-certificate=bin\cacert.pem -d tmp -i

RD /S /Q tmp
MKDIR tmp

php lib\generar-categorias-urls.php
%ARIA2CD% tmp\categorias-urls.txt > tmp\categorias-urls.log

php lib\generar-paginas-urls.php
%ARIA2CD% tmp\paginas-urls.txt > tmp\paginas-urls.log

php lib\generar-productos-feed.php

COPY "tmp\productos.csv" "productos.csv" /Y
COPY "tmp\productos.rss" "productos.rss" /Y
